import os
import utils1.logging_api as logger # requires utils/loggin_api.py
import datetime
import traceback
import pymssql
import json
from flask import Flask
from flask import render_template, request, redirect, url_for

app = Flask(__name__)

def init_logger():
    with open('D:/temp/logs/user_conf.json') as json_file:
        conf = json.load(json_file)
        #log_file_location=D:\temp\logs\ebay.log'\n'
        #log_level=DEBUG
        logger.init(f'{conf["log_file_location"]}'+
                    f'{datetime.datetime.now().year}_'+
                    f'{datetime.datetime.now().month}_' +
                    f'{datetime.datetime.now().day}_' +
                    f'{datetime.datetime.now().hour}_' +
                    f'{datetime.datetime.now().minute}_' +
                    f'{datetime.datetime.now().second}' + '.log'
                    , conf["log_level"])

def test_db_connection():
    try:
        logger.write_lo_log(f'Testing connection to [{conf["server"]}] [{conf["database"]}]', 'INFO')
        conn = pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database'])
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to connecto to db [{conf["server"]}] [{conf["database"]}]', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {e}', 'ERROR')
        logger.write_lo_log(f'Failed to connecto to db {tr}', 'ERROR')
        print('Faild to connect to db ... exit')
        exit(-1)

@app.route('/customers', methods = ['POST'])
def post_new_customre():
    logger.write_lo_log('/customers POST', 'INFO')
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            new_customer = request.get_json()
            print(new_customer)
            logger.write_lo_log(f'/customers POST new customer {new_customer}', 'INFO')
            query = 'INSERT INTO CUSTOMERS (NAME, AGE, ADDRESS, SALARY) '+\
                               f"VALUES ('{new_customer['name']}', {new_customer['age']}, '{new_customer['address']}', {new_customer['salary']});"
            logger.write_lo_log(f'/customers POST new customer query {query}', 'DEBUG')
            conn.execute_query(query)
            return json.dumps({'result' : 'succeed'})

    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})

@app.route('/customers/<int:id>', methods = ['GET'])
def get_by_id(id):
    query = ''
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            query = f'SELECT * FROM CUSTOMERS WHERE ID={id}'
            conn.execute_query(query)
            result = []
            for row in conn:
                print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')
                # SALARY requires exytra parsing
                result = {'ID': row["ID"], 'NAME': row["NAME"], 'AGE' : row["AGE"]}
            print(result)
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [{query}] to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})

@app.route('/customers', methods = ['GET'])
def get_all_customres():
    try:
        with pymssql._mssql.connect(server=conf['server'], user='', password='',
                                    database=conf['database']) as conn:
            conn.execute_query('SELECT * FROM CUSTOMERS')
            result = []
            for row in conn:
                print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')
                #result.append({ 'ID' : row["ID"], 'NAME' : row["NAME"], 'AGE' : row["AGE"], 'ADDRESS' : row["ADDRESS"], 'SALARY' : row["SALARY"]})
                result.append({'ID': row["ID"], 'NAME': row["NAME"]})

            print('=================== was pymssql._mssql connector')
            print(result)
            return json.dumps(result)
    except Exception as e:
        tr = traceback.format_exc()
        logger.write_lo_log(f'Failed to run [SELECT * FROM CUSTOMERS] to db {tr}', 'ERROR')
        return json.dumps({'Error' : e})

def main():
    init_logger()
    logger.write_lo_log('**************** System started ...', 'INFO')

    #test_db_connection()

with open('D:/temp/logs/user_conf.json') as json_file:
    conf = json.load(json_file)
main()


app.run(debug=True)