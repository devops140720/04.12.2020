import os
import utils1.logging_api as logger # requires utils/loggin_api.py
import datetime
import traceback
import pymssql
import json

with open('user_conf.json') as json_file:
    conf = json.load(json_file)

with pymssql.connect(server=conf['server'], user='', password='', database=conf['database']) as conn:
    cursor = conn.cursor(as_dict=True)

    # this will run ONCE !!!!!!!
    cursor.execute("""
            CREATE PROCEDURE FindCustomer
                @name VARCHAR(100)
            AS BEGIN
                SELECT * FROM CUSTOMERS WHERE NAME = @name
            END 
        """
        )
    conn.commit()

    # this will run many times
    cursor.callproc('FindCustomer',('DAVID',))
        for row in cursor:
            print(f'{row["ID"]} {row["NAME"]} {row["AGE"]} {row["ADDRESS"]} {row["SALARY"]}')